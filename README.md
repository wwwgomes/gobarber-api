# GoBarber RESTful API

RESTful API (Application GoBarber)

## Tecnologias utilizadas

Foram utilizadas nesta implementação as seguintes tecnologias

| Tecnologias | Descrição                                                        |
|-------------|------------------------------------------------------------------|
| Postgres    | Banco de dados relacional para armazenamento de dados            |
| MongoDB     | Para armazenamentos de dados sem nescessidade de relacionamentos |
| Redis       | Para uso nas filas de email                                      |
| Sentry      | Para monitoriamento de logs em produção                          |
| JWT         |  Para geração de token para a autenticação                       |
| Mailtrap    | Para emular os envios de email                                   |
| Handlebars  | Para criação de templates dos emails                             |

Entre outras...
